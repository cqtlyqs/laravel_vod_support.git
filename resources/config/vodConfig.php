<?php
/**
 * 阿里云点播相关的配置
 */
return [
    "vod_account_id"        => "", // 阿里云RMA账号ID
    "vod_access_key"        => "",
    "vod_access_key_secret" => "",
    "vod_regionId"          => "",
    "play_domain"           => "",
    "work_flow_id"          => "", //阿里点播上传后转码的工作流id
];
