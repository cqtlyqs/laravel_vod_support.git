# laravel_vod_support

#### 介绍
Laravel的视频组件
集成阿里云点播相关接口

#### 使用说明

1.引入
```
composer require ktnw/vod_support
```
2.发布
```
php artisan vendor:publish --provider="Ktnw\VodSupport\Providers\VodSupportServiceProvider"
```
3.修改相关class的namespace
```
php artisan support:vod
```
4.根据实际业务，自行扩展。具体示例请查看VodController。
