<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * Date: 2022/02/17
 * Time: 17:54
 */

namespace Ktnw\VodSupport\Utils;


class UploadVideoQuery
{
    /**
     * 视频源文件名。必传。
     * 示例：D:\****.mp4。
     * 必须带扩展名，且扩展名不区分大小写。
     * 支持格式：AVI格式：AVI / MP4
     *
     */
    private $fileName;

    /**
     * 视频标题。必传。
     * 长度不超过128个字符。
     * UTF-8编码。
     */
    private $title;

    /**
     * 自定义视频封面的URL地址。
     *  示例：https://*****test.cn/image/D22F553*****TEST.jpeg
     */
    private $coverUrl;


    /**
     * 自定义视频封面的URL地址。
     *  视频描述。
     * 长度不超过1024个字符。
     * UTF-8编码。
     */
    private $description;

    /**
     * 视频文件大小。单位：字节。
     * UTF-8编码。
     */
    private $fileSize;

    /**
     * 视频分类ID。
     * 登录点播控制台，选择配置管理 > 媒资管理配置 > 分类管理编辑或查看分类的ID。
     */
    private $cateId;

    /**
     * 转码模板组ID。
     * 说明:
     *  当不为空时，会使用该指定的模板组进行转码。登录点播控制台，选择配置管理 > 媒体处理配置 > 转码模板组查看模版组ID。
     */
    private $templateGroupId;

    /**
     * 存储地址。
     * 说明:
     * 当不为空时，会使用该指定的存储地址上传视频文件。登录点播控制台，选择配置管理 > 媒资管理配置 > 存储管理查看存储地址。
     */
    private $storageLocation;

    /**
     * 应用ID。默认取值：app-1000000。
     */
    private $appId = "app-1000000";

    /**
     * UploadVideoQuery constructor.
     */
    public function __construct()
    {
        // 默认上传目录
        $this->cateId = "";
        // 默认转码模板组：通用不加密_流畅标清高清
        $this->templateGroupId = "49e2c2e81e4e3e5bf9ec2b1b0c9a78db";
        $this->coverUrl        = "";
        $this->description     = "";
        $this->fileSize        = "";
        $this->storageLocation = "";
        $this->fileName        = "";
    }


    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * @param mixed $coverUrl
     */
    public function setCoverUrl($coverUrl): void
    {
        $this->coverUrl = $coverUrl;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param mixed $fileSize
     */
    public function setFileSize($fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return mixed
     */
    public function getCateId()
    {
        return $this->cateId;
    }

    /**
     * @param mixed $cateId
     */
    public function setCateId($cateId): void
    {
        $this->cateId = $cateId;
    }

    /**
     * @return mixed
     */
    public function getTemplateGroupId()
    {
        return $this->templateGroupId;
    }

    /**
     * @param mixed $templateGroupId
     */
    public function setTemplateGroupId($templateGroupId): void
    {
        $this->templateGroupId = $templateGroupId;
    }

    /**
     * @return mixed
     */
    public function getStorageLocation()
    {
        return $this->storageLocation;
    }

    /**
     * @param mixed $storageLocation
     */
    public function setStorageLocation($storageLocation): void
    {
        $this->storageLocation = $storageLocation;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param mixed $appId
     */
    public function setAppId($appId): void
    {
        $this->appId = $appId;
    }


}