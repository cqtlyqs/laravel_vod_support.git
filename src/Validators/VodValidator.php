<?php

namespace Ktnw\VodSupport\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class VodValidator.
 */
class VodValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        "getPlayAuth"        => [
            "videoId"     => "required|max:100",
            "previewTime" => "nullable|integer|min:0",
        ],
        "getUploadVideoAuth" => [
            "title"  => "required|max:128",
            "cateId" => "required",
        ],
        "refreshUploadVideo" => [
            "videoId" => "required",
        ],
        "deleteUploadVideo"  => [
            "requestId" => "required",
            "videoId"   => "required",
        ],
    ];

}
